package poli4;

import java.util.ArrayList;
import java.util.Hashtable;

public class BankProc {

    public void insertRaportDataBase(Raport c) 
    {

        ConexiuneDb.getInstance();
        MySqlConnection _conn = ConexiuneDb.getConexion();
        String sql = "INSERT into raport (Id, Data,Tip_Actiune,Nume_Client) values ('" +c.IdP+ "',@data_c,'" +c.TipActP+ "','"+c.NumeClient+"')";
        try
        {
            _conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, _conn);
            cmd.Parameters.AddWithValue("@data_c", Convert.ToDateTime(c.DataP));
            cmd.ExecuteNonQuery();
            _conn.Close();
            MessageBox.Show("Actiune incheiata cu succes!");
        }
        catch (MySqlException e)
        {
            Console.WriteLine(e.Message);
            _conn.Close();
            MessageBox.Show(e.Message);
        }
    }




    public DataTable getRaport(int id,String data1, String data2)
    {

        DataTable dt = new DataTable();
        dt.Clear();
        ConexiuneDb.getInstance();
        MySqlConnection _conn = ConexiuneDb.getConexion();
        String sql = "SELECT * FROM raport WHERE (Data BETWEEN @dataa AND @datab) AND  Id='"+id+"'";
       // String sql = "SELECT * FROM raport WHERE   Id='" + id + "'";
        try
        {
            MySqlCommand cmd = new MySqlCommand(sql, _conn);
            cmd.Parameters.AddWithValue("@dataa", Convert.ToDateTime(data1));
            cmd.Parameters.AddWithValue("@datab", Convert.ToDateTime(data2));
            _conn.Open();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            {
                da.Fill(dt);

            }

            _conn.Close();
            da.Dispose();
            MessageBox.Show("Actiune incheiata cu succes!");

        }
        catch (MySqlException e)
        {
            Console.WriteLine(e.Message);
            _conn.Close();
            MessageBox.Show(e.Message);
        }

        return dt;
    }

}
}


}
