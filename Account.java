package poli4;

public class Account {


    class AngajatOperations
    {
         private ContGateway contData;
         private ClientGateway clientData;
         private FindClient findClient;
         private FindCont findCont;
         private FindAngajat findAngajat;
         private int id;
         private RaportGateway raportData; 

        public AngajatOperations(int _id)
        {
           contData = new ContGateway();
           clientData = new ClientGateway();
           findClient = new FindClient();
           findCont = new FindCont();
           findAngajat = new FindAngajat();
           raportData = new RaportGateway();
           this.id = _id;
        }
        public AngajatOperations()
        {
            contData = new ContGateway();
            clientData = new ClientGateway();
            findClient = new FindClient();
            findCont = new FindCont();
            findAngajat = new FindAngajat();
            raportData = new RaportGateway();
        }



        public void adaugaClient(String[] s)
        {
            Client c = new Client(s[0],s[1],s[2],s[3],Convert.ToInt32(s[4]),s[5]);
           
            if (c.getCnp.Length == 13)
            {
                if (c.NumarCardProp.Length == 16)
                {
                    clientData.insertClientDataBase(c);
                }
                else MessageBox.Show("Numarul cardului trebuie sa fie de lungime 16!");
            }
            else MessageBox.Show("Cnp-ul trebuie sa fie de lungime 13!");

        }



        public void updateClient(String[] s)
        {

            Client c = new Client(s[0], s[1], s[2], s[3], Convert.ToInt32(s[4]), s[5]);
            if (c.getCnp.Length == 13)
            {
                if (c.NumarCardProp.Length == 16)
                {
                    clientData.updateClientDataBase(c);
                }
                else MessageBox.Show("Numarul cardului trebuie sa fie de lungime 16!");
            }
            else MessageBox.Show("Cnp-ul trebuie sa fie de lungime 13!");

        }

        public DataTable viewTabelClienti()
        {
            return clientData.selectClientDataBase();
        }

        public void adaugaCont(String[] s)
        {
            Cont c = new Cont(Convert.ToInt32(s[0]), s[1], Convert.ToDouble(s[2]), s[3], Convert.ToInt32(s[4]));
            contData.insertContDataBase(c);
        }
        public void updateCont(String[] s)
        {

            Cont c = new Cont(Convert.ToInt32(s[0]), s[1], Convert.ToDouble(s[2]), s[3], Convert.ToInt32(s[4]));
            contData.updateContDataBase(c);

        }



        public void deleteCont(String[] s)
        {

            Cont c = new Cont(Convert.ToInt32(s[0]), s[1], Convert.ToDouble(s[2]), s[3], Convert.ToInt32(s[4]));
            contData.deleteContMap(c);

        }
      
     



}
