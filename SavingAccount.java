/**
 * 
 */
package poli4;

/**
 * @author Lenovo
 *
 */
public class SavingAccount {

	  
    private AngajatGateway angajatData;
    private UserGateway userData;
    private FindAngajat findAngajat;
    private RaportGateway raportData;

    public AdministratorOperations()
    {
        angajatData = new AngajatGateway();
        userData = new UserGateway();
        findAngajat = new FindAngajat();
        raportData = new RaportGateway();
    }

    public string CreatePassword(int length)
    {
        const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder res = new StringBuilder();
        Random rnd = new Random();
        while (0 < length--)
        {
            res.Append(valid[rnd.Next(valid.Length)]);
        }
        return res.ToString();
    }
    public void deleteUser(String s)
    {
        userData.deleteUser(s);
    }


     	


private string getMd5Hash(string input)
    {
        // Create a new instance of the MD5CryptoServiceProvider object.
        MD5 md5Hasher = MD5.Create();

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }


    public int getIdAngajat(String user, String pass)
    {
        String password = getMd5Hash(pass);
        return findAngajat.getAngajatUP(user, password);
    }

    public int getIdAngajatCnp(String cnp) 
    {
        return findAngajat.getAngajatCnp(cnp);
    }
    public void addAngajatToDatabase(String[] s)
    {

     
        string pass = getMd5Hash(s[3]);
        Angajat a = new Angajat(s[0],s[1],s[2],pass,s[5],s[6]);
        User u = new User(a.UsernameP, a.PasswordP, s[6]);
        if (userData.findUsername(u) == false)
        {
           // angajatData.insertAngajatDataBase(a);
            angajatData.addAngajatMap(a);
            userData.insertUserDataBase(u);
        }
        else MessageBox.Show("Username-ul exista deja!");
      
    }
    public void updateAngajatToDatabase(String[] s,int id)
    {

       // angajatData.updateAngajat(s);
        Angajat a = new Angajat(s[0], s[1], s[2], s[3],s[5],s[6]);
        angajatData.updateAngajatDataBase(a,id);

    }

    public void deleteAngajatFromDatabase(String[] s,int id)
    {
        Angajat a = new Angajat(s[0], s[1], s[2], s[3],s[5],s[6]);
       angajatData.deleteAngajatDataBase(a,id);
       // angajatData.deleteAngajat(s);

    }

    public DataTable viewTabelAngajati()
    {
        return angajatData.selectTabelAngajat();
    }

    public DataTable viewTabelAngajatiCNP(String cnp)
    {
        return findAngajat.getAngajatTabel(cnp);
    }

    public DataTable viewRaport(int id, String data1, String data2)
    {
        return raportData.getRaport(id,data1,data2);
    }

    public String login(String u, String p)
    {
        String pass = getMd5Hash(p);
        return userData.getUser(u, pass);
    }

    public String numeAngajatCnp(String cnp)
    {
        return findAngajat.getNumeAngajatCnp(cnp);
    }
}

}
