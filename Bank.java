package poli4;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
public class Bank {

	namespace WindowsFormsApplication1.Model
	{
	    using System;
	    using System.Data.Entity;
	    using System.Data.Entity.Infrastructure;
	    
	    public partial class bancaEntities1 : DbContext
	    {
	        public bancaEntities1()
	            : base("name=bancaEntities1")
	        {
	        }
	    
	        protected override void OnModelCreating(DbModelBuilder modelBuilder)
	        {
	            throw new UnintentionalCodeFirstException();
	        }
	    
	        public DbSet<angajat> angajats { get; set; }
	        public DbSet<clienti> clientis { get; set; }
	        public DbSet<contclient> contclients { get; set; }
	        public DbSet<raport> raports { get; set; }
	        public DbSet<user> users { get; set; }
	    }
	}

	
	using System;
    using System.Collections.Generic;
    
    public partial class contclient
    {
        public int Id_cont { get; set; }
        public string Tip { get; set; }
        public double Suma { get; set; }
        public System.DateTime Data_creare { get; set; }
        public int Id_client { get; set; }
    }
}

