package poli4;

public class GUI {

	   public partial class OperatiiClient : Form
	    {
	        private int id;
	        private AngajatOperations newc; 

	        public OperatiiClient(int _id)
	        {
	            InitializeComponent();
	            this.id = _id;
	            newc = new AngajatOperations(id);
	        }

	        private void OperatiiClient_Load(object sender, EventArgs e)
	        {
	           this.clientiTableAdapter.Fill(this.bancaDataSet2.clienti);
	        }

	      

	        private void Insert_Click(object sender, EventArgs e)
	        {
	            String[] str = new String[6];
	            int row = dataGridView1.CurrentRow.Index;
	            for (int i = 0; i < 6; i++)
	            {
	                str[i] = dataGridView1.Rows[row].Cells[i].Value.ToString();
	            }
	           // AngajatOperations newc = new AngajatOperations(id);
	            if (newc.getIdClientCnp(str[2]) == 0)
	            {
	                newc.adaugaClient(str);
	                String tipActiune = "Adaugare client";
	                // String nume = newc.getClientId(Convert.ToInt32(str[4]));  
	                String nume = str[0] + " " + str[1];
	                newc.insereazaRaport(tipActiune, nume);
	            }
	            else MessageBox.Show("Clientul exista deja!");

	        }

	        //private void Update_Click(object sender, EventArgs e)
	        //{

	        //    String[] str = new String[6];
	        //    int row = dataGridView1.CurrentRow.Index;
	        //    for (int i = 0; i < 6; i++)
	        //    {
	        //        str[i] = dataGridView1.Rows[row].Cells[i].Value.ToString();
	        //    }
	        //    AngajatOperations newc = new AngajatOperations();
	        //    newc.updateClient(str);
	        //    Console.WriteLine("aici avem" + str.ToList().ToString());

	        //}

	        private void View_Click(object sender, EventArgs e)
	        {
	            TabelClienti tbCl = new TabelClienti(id);
	            tbCl.Show();
	        }

	        private void buttonupdate_Click(object sender, EventArgs e)
	        {
	            String[] str = new String[6];
	            int row = dataGridView1.CurrentRow.Index;
	            for (int i = 0; i < 6; i++)
	            {
	                str[i] = dataGridView1.Rows[row].Cells[i].Value.ToString();
	            }
	           // AngajatOperations newc = new AngajatOperations();
	            newc.updateClient(str);

	        }

	        private void pictureBox1_Click(object sender, EventArgs e)
	        {

	        }

	        
	    }
	}


}
