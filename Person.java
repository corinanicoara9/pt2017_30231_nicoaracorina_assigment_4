package poli4;
import java.io.Serializable;
public class Person {

	class Client
    {
        private String nume;
        private String prenume;
        private String cnp;       
        private String adresa;
        private int Id_client;      
        private String numarCArd;



        

        public Client(String u, String p, String newcnp, String newadresa, int Id, String numarcard)
        {
            this.nume = u;
            this.prenume = p;
            this.cnp = newcnp;
            this.adresa = newadresa;
            this.Id_client = Id;
            this.numarCArd = numarcard;
                    

        }



        public String getAdresa
        {
            get { return adresa; }
            set { adresa = value; }
        }
        public int getIdClient
        {
            get { return Id_client; }
            set { Id_client = value; }
        }

        public String NumarCardProp
        {
            get { return numarCArd; }
            set { numarCArd = value; }
        }

        public String getCnp
        {
            get { return cnp; }
            set { cnp = value; }
        }
        public String getPrenume
        {
            get { return prenume; }
            set { prenume = value; }
        }

        public String getNume
        {
            get { return nume; }
            set { nume = value; }
        }

    }
}
